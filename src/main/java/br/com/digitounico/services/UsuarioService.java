package br.com.digitounico.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.digitounico.models.Usuario;
import br.com.digitounico.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	public List<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}
	
	public Usuario buscarUsuario(Integer id) {
		Optional<Usuario> usuarioOpt = usuarioRepository.findById(id);
		
		return usuarioOpt.isPresent() ? usuarioOpt.get() : null;
	}
	
	public void salvarUsuario(Usuario usuario) {
		usuarioRepository.save(usuario);
	}
	
	public void editarUsuario(Usuario usuario) {
		usuarioRepository.save(usuario);
	}
	
	public void deletarUsuario(Integer id) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
		if (usuarioOptional.isPresent()) {
			usuarioRepository.delete(usuarioOptional.get());
		}
	}
	
}
