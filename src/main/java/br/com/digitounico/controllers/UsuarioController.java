package br.com.digitounico.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.digitounico.models.Usuario;
import br.com.digitounico.services.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Component
@Api(value="Usuario", description="Esta API fornece informações sobre os usuários do sistema.")
@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	
	@ApiOperation(value = "Lista todos os usuários cadastrados.",
		    notes = "")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = Usuario.class, responseContainer = "List", message = "Usuarios")
			})
	@GetMapping(value = {""})
	public ResponseEntity<?> buscarUsuarios() {

		List<Usuario> listaUsuarios = usuarioService.listarUsuarios();;

		if (listaUsuarios == null || listaUsuarios.isEmpty()) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(listaUsuarios);
	}
	
	@ApiOperation(value = "Recupera um usuario dado seu id.",
		    notes = "")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = Usuario.class, message = "Usuario")
			})
	@GetMapping(value = {"/id/{id}"})
	public ResponseEntity<?> buscarUsuarioPorId(@PathVariable("id") String id) {

		Usuario usuario = null;
		if(!StringUtils.isBlank(id)) {
			usuario = usuarioService.buscarUsuario(Integer.parseInt(id));
		} else {
			ResponseEntity.badRequest().body("O campo 'Id' é obrigatório.");
		}

		if (usuario == null) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(usuario);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Criar um novo usuário.")
	@ApiResponses(value = {
			@ApiResponse(code = 201, response = String.class, message = "Usuário salvo com sucesso.")
			})
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> salvarUsuario(@RequestBody Usuario usuario) {
		usuarioService.salvarUsuario(usuario);
		return ResponseEntity.ok("Usuário salvo com sucesso.");
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Atualizar um usuário existente.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Usuário atualizado com sucesso.")
			})
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> atualizarUsuario(@RequestBody Usuario usuario) {
		usuarioService.salvarUsuario(usuario);
		return ResponseEntity.ok("Usuário atualizado com sucesso.");
	}
	
	@DeleteMapping(value = {"/id/{id}"})
	@ApiOperation(value = "Deletar um usuário existente.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Usuário deletado com sucesso.")
			})
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> deletarrUsuario(@PathVariable("id") String id) {
		usuarioService.deletarUsuario(Integer.parseInt(id));
		return ResponseEntity.ok("Usuário deletado com sucesso.");
	}
	
}
