package br.com.digitounico.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "calculos")
public class Calculo implements Serializable {

	private static final long serialVersionUID = -3883992297431974086L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer calculoId;

	Integer inteiro;
	
	Integer contador;
	
	@ManyToMany
	Set<Usuario> usuarios;

	public Integer getCalculoId() {
		return calculoId;
	}

	public void setCalculoId(Integer calculoId) {
		this.calculoId = calculoId;
	}

	public Integer getInteiro() {
		return inteiro;
	}

	public void setInteiro(Integer inteiro) {
		this.inteiro = inteiro;
	}

	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calculoId == null) ? 0 : calculoId.hashCode());
		result = prime * result + ((contador == null) ? 0 : contador.hashCode());
		result = prime * result + ((inteiro == null) ? 0 : inteiro.hashCode());
		result = prime * result + ((usuarios == null) ? 0 : usuarios.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Calculo other = (Calculo) obj;
		if (calculoId == null) {
			if (other.calculoId != null)
				return false;
		} else if (!calculoId.equals(other.calculoId))
			return false;
		if (contador == null) {
			if (other.contador != null)
				return false;
		} else if (!contador.equals(other.contador))
			return false;
		if (inteiro == null) {
			if (other.inteiro != null)
				return false;
		} else if (!inteiro.equals(other.inteiro))
			return false;
		if (usuarios == null) {
			if (other.usuarios != null)
				return false;
		} else if (!usuarios.equals(other.usuarios))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Calculo [calculoId=" + calculoId + ", inteiro=" + inteiro + ", contador=" + contador + ", usuarios="
				+ usuarios + "]";
	}
	
}
