package br.com.digitounico.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.digitounico.models.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
