package br.com.digitounico.calculadoras;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DigitoUnicoCalculadoraTest {

	@DisplayName("Teste do método criarInteiro")
	@Test
	public void teste_criarInteiro() {
		DigitoUnicoCalculadora calculadora = new DigitoUnicoCalculadora();
		
		Assertions.assertEquals(9875L, calculadora.criarInteiro("9875", 1L));
		Assertions.assertEquals(9875987598759875L, calculadora.criarInteiro("9875", 4L));
	}
	
//	@DisplayName("Teste do método digitoUnico")
//	@Test
//	public void teste_calcular_digitoUnico() {
//		DigitoUnicoCalculadora calculadora = new DigitoUnicoCalculadora();
//		
//		Assertions.assertEquals(2L, calculadora.digitoUnico("9875", 1L));
//		Assertions.assertEquals(8L, calculadora.digitoUnico("9875", 4L));
//	}
	
}
